package Coding_challenge_day_1;

import java.util.Iterator;
import java.util.Scanner;

public class Day1 {
	
	public static void IncreasingArr(String [] tasks) {
		System.out.println("");
		System.out.println("Show arr in increasing order:");
		for (int i = 0; i < tasks.length; i++) {
			System.out.println("Task " + (i + 1) + ": " + tasks[i]);
		}
	}
	
	public static void DecreasingArr(String [] tasks) {
		System.out.println("");
		System.out.println("Show arr in decreasing order:");
		for (int i = tasks.length - 1; i >= 0; i--) {
			System.out.println("Task " + (i + 1) + ": " + tasks[i]);
		}
	}
	
	public static void IncreasingTasks(String [] tasks) {
		System.out.println("");
		System.out.println("Show the tasks in increasing order:");

		for (int i = 0; i < tasks.length; i++) {
			for (int j = i + 1; j < tasks.length; j++) {
				if (tasks[i].charAt(0) > tasks[j].charAt(0)) {
					String tmpString = tasks[i];
					tasks[i] = tasks[j];
					tasks[j] = tmpString;
				}
			}
		}
		for (String s : tasks) {
			System.out.print(s + " - ");
		}
	}
	
	public static void DecreasingTasks(String [] tasks) {
		System.out.println("");
		System.out.println("Show the tasks in decreasing order:");

		for (int i = 0; i < tasks.length; i++) {
			for (int j = i + 1; j < tasks.length; j++) {
				if (tasks[i].charAt(0) < tasks[j].charAt(0)) {
					String tmpString = tasks[i];
					tasks[i] = tasks[j];
					tasks[j] = tmpString;
				}
			}
		}
		for (String s : tasks) {
			System.out.print(s + " - ");
		}
	}
	
	public static void CheckRepeat(String [] tasks) {
		String str = "";
		for (int i = 0; i < tasks.length - 1; i++) {
			for (int j = i + 1; j < tasks.length; j++) {
				if (tasks[i].equals(tasks[j])) {
					String[] str1 = str.split(" / ");
					int dem = 0;
					for (String s : str1) {
						if (s.equals(tasks[i])) {
							// if the task already exists, dem++ and exit for loop
							dem++;
							break;
						}
					}
					if (dem == 0) {
						if (str.isEmpty()) {
							str += tasks[i];
						} else {
							str += " / " + tasks[i];
						}

					}
				}
			}
		}
		
		if(str.isEmpty()) {
			System.out.println("The tasks aren't repeated");
		}
		else {
			System.out.println(str + " - the tasks are repeated");
		}
	}
	public static void main(String[] args) {
		System.out.println("Name: Dao Thi Lan Phuong");

		Scanner src = new Scanner(System.in);
		System.out.println("Number of the tasks for the day: ");
		int x = src.nextInt();
		String [] tasks  = new String [x];
		// String tasks[] = {"Establish a routine", "Exercise", "Check my progress",
		// "Spend time with family", "Plan tomorrow -- tonight"};
		
		// Insert tasks
		tasks = new String[x];
		for (int i = 0; i < x; i++) {
			System.out.print("Task " + (i + 1) + ": ");
			tasks[i] = src.next();
		}

		// Show arr in increasing order.
		IncreasingArr(tasks);

		// Show arr in decreasing order.
		DecreasingArr(tasks);

		// Show the tasks in increasing order.
		IncreasingTasks(tasks);

		// Show the tasks in decreasing order.
		DecreasingTasks(tasks);

		System.out.println("\n");
		// Check the repeat - Show the tasks are repeated
		CheckRepeat(tasks);
	}

}
