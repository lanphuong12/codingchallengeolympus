package Coding_challenge_day_3;

import java.util.ArrayList;
import java.util.Scanner;

public class Day3 {

	public Scanner sc = new Scanner(System.in);
	public Task tsTasks = new Task();
	public ArrayList<Task> arrTasks = new ArrayList<>();

	public void menu() {
		System.out.println("Menu");
		System.out.println("1. AddMore ");
		System.out.println("2. Update ");
		System.out.println("3. Delete ");
		System.out.println("4. Search ");
		System.out.println("5. Display ");
		System.out.println("0. Exit ");
		System.out.println("Enter your choice: ");
		int ch = Integer.parseInt(sc.nextLine());
		switch (ch) {
		case 0:
			break;
		case 1:
			tsTasks.AddData(arrTasks);
			menu();
			break;
		case 2:
			tsTasks.Update(arrTasks);
			menu();
			break;
		case 3:
			tsTasks.Delete(arrTasks);
			menu();
			break;
		case 4:
			tsTasks.DisplaySearch(arrTasks);
			menu();
			break;
		case 5:
			tsTasks.Display(arrTasks);
			menu();
			break;
		default:
			System.out.println("Incorrect input format!");
			menu();
			break;
		}
	}

	public static void main(String[] args) {
		Day3 day3 = new Day3();
		day3.menu();
		System.out.println("Finish");

	}

}
