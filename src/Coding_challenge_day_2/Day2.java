package Coding_challenge_day_2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;

public class Day2 {

	private static void AddTask(String[] arr) {
		Scanner scanner = new Scanner(System.in);
		String tt;
		do {
			String[] newArr = new String[arr.length + 1];
			System.out.print("Task " + (arr.length + 1) + ": ");
			String newTask = scanner.next();
			for (int i = 0; i < arr.length; i++) {
				newArr[i] = arr[i];
			}
			newArr[newArr.length - 1] = newTask;
			arr = newArr;
			System.out.println("");
			System.out.println("Add successful");
			Display(arr);
			System.out.println("Do you want to continous? Y or N");
			tt = scanner.next();
		} while (tt.equals("Y"));
		Menu(arr);
	}

	private static void UpdateTask(String[] arr) {
		Scanner scanner = new Scanner(System.in);
		int numbertask;
		String tt;
		do {
			do {
				System.out.print("Task you want to update: ");
				numbertask = scanner.nextInt();
				if (numbertask < 0 && numbertask >= arr.length) {
					System.out.println("Your task doesn't exist. Please try again!");
				}
			} while (numbertask < 0 && numbertask >= arr.length);
			System.out.print("Update your task below: \n");
			System.out.print("Task " + arr[numbertask] + ": ");
			String newTask = scanner.next();
			arr[numbertask] = newTask;
			System.out.println("Update successful");
			Display(arr);
			System.out.println("Do you want to continous? Y or N");
			tt = scanner.next();
		} while (tt.equals("Y"));
		Menu(arr);
	}

	private static void DeleteTask(String[] arr) {
		Scanner scanner = new Scanner(System.in);
		int numbertask;
		String tt;
		do {
			do {
				System.out.print("Task you want to delete: ");
				numbertask = scanner.nextInt();
				if (numbertask < 0 && numbertask >= arr.length) {
					System.out.println("Your task doesn't exist. Please try again!");
				}
			} while (numbertask < 0 && numbertask >= arr.length);
			System.out.println("Are you sure? If you want to delete, enter 1");
			int check = scanner.nextInt();
			if (check == 1) {

				for (int i = numbertask; i < arr.length-1; i++) {
					arr[i] = arr[i + 1];
				}
				String[] newArr = new String[arr.length - 1];
				for (int i = 0; i < arr.length -1; i++) {
					newArr[i] = arr[i];
				}
				arr = newArr;
			} else {
				Menu(arr);
			}
			System.out.println("Delete successful");
			Display(arr);
			System.out.println("Do you want to continous? Y or N");
			tt = scanner.next();
		} while (tt.equals("Y"));
		Menu(arr);
	}

	private static void SearchTask(String[] arr) {
		Scanner scanner = new Scanner(System.in);
		String tt;
		do {
			System.out.println("Enter the task you want to search: ");
			String searchTask = scanner.nextLine();
			for (int i = 0; i < arr.length; i++) {
				if (arr[i].indexOf(searchTask) == 0) {
					System.out.println("I can't find it");
				} else {
					System.out.println("Task " + (i + 1) + ": " + arr[i]);
				}
			}
			System.out.println("Do you want to continous? Y or N");
			tt = scanner.next();
		} while (tt.equals("Y"));
		Menu(arr);
	}

	private static void Display(String[] arr) {
		int c = 0;
		while (c < arr.length) {
			System.out.println("Task " + (c + 1) + ": " + arr[c]);
			c++;
		}
	}

	public static void Menu(String[] arr) {
		do {
			Scanner scanner = new Scanner(System.in);
			System.out.println("Menu");
			System.out.println("1. Add ");
			System.out.println("2. Update ");
			System.out.println("3. Delete ");
			System.out.println("4. Search ");
			System.out.println("0. Exit ");
			System.out.print("Enter your choice: ");
			int ch = Integer.parseInt(scanner.nextLine());
			switch (ch) {
			case 0:
				break;
			case 1:
				AddTask(arr);
				break;
			case 2:
				UpdateTask(arr);
				break;
			case 3:
				DeleteTask(arr);
				break;
			case 4:
				SearchTask(arr);
				break;
			default:
				System.out.println("Incorrect input format!");
				break;
			}
		} while (true);

	}

	public static void main(String[] args) {

		String[] arrTask = { "Establish a routine", "Exercise", "Check my progress", "Spend time with family",
				"Plan tomorrow -- tonight" };

		Menu(arrTask);
//		// Add task in arr
//		AddTask(arrTask);
//		//Display(arrTask);
//		System.out.println("\n");
//
//		// Update task in arr
//		UpdateTask(arrTask);
//		System.out.println("\n");
//
//		// Delete task in arr
//		DeleteTask(arrTask);
//		System.out.println("\n");
//
//		// Search task in arr
//		SearchTask(arrTask);
	}

}
